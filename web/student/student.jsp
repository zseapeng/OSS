<%@ page import="com.zjnu.oss.model.Student" %><%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/24
  Time: 8:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="../header.html"%>
    <title>student</title>
</head>
<body>
<%@include file="nav.jsp"%>
<%!
    String picurl = "../images/photo.jpg";
%>
<%
    Student student = (Student) session.getAttribute("LoginUser");
    if (student.getT_picture()!=null&&!student.getT_picture().equals("")){
        picurl = student.getT_picture();
    }
%>
<div class="container" style="margin-top: 5px">
    <div class="row ">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img src="<%=picurl%>" alt="头像" class="img-responsive center-block">
                <div class="caption">
                    <form name="itemForm"   id="itemForm" method="post" action="/student/UpLoadPicServlet" enctype="multipart/form-data" onsubmit="checkForm()">
                        <div class="form-group">
                            <label >选择头像</label>
                            <input type="file" name="fileName">
                            <p class="help-block">请选择头像</p>
                        </div>
                        <input type="submit" value="提交" class="btn btn-default center-block"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-offset-3" >
        <div class="col-sm-offset-2 col-sm-6">
            <p>
                <label class="h4">姓名：<%=student.getT_name()%></label>
            </p>
            <p>
                <label class="h4">班级：<%=student.getT_class()%></label>
            </p>
            <p>
                <label class="h4">专业：<%=student.getT_major()%></label>
            </p>
            <p>
                <label class="h4">性别：<%=student.getT_gender()%></label>
            </p>
        </div>
        </div>
    </div>
</div>
</body>
</html>
