<%@ page import="com.zjnu.oss.model.Student" %>
<%@ page import="org.apache.ibatis.session.SqlSession" %>
<%@ page import="com.zjnu.oss.util.MyBatisUtil" %>
<%@ page import="com.zjnu.oss.model.Introduction" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <title>无标题文档</title>
    <link href="../css/tiankong.css" rel="stylesheet" type="text/css">
    <%@include file="nav.jsp"%>
    <style type="text/css">
        .all{font:normal 15px/1.5 Arial, Helvetica, Free Sans, sans-serif;color: #222;overflow-y:scroll;padding:60px 0 0 0;}
        #my-form{width:755px;margin:0 auto;border:1px solid #ccc;padding:3em;border-radius:3px;box-shadow:0 0 2px rgba(0,0,0,.2);}
        #comments{width:350px;height:100px;}
    </style>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <div class="center-block a_log">
        <img src="../images/zjun.jpg" class="img-responsive img-circle tm-border center-block" alt="templatemo easy profile">
    </div>
</head>
<body>
<%
    Student student = (Student) session.getAttribute("LoginUser");
    String username = student.getT_username();
    SqlSession sqlSession = MyBatisUtil.createSession();
    Introduction introduction = sqlSession.selectOne(Introduction.class.getName()+".selectinfo",username);
    MyBatisUtil.closeSqlSession(sqlSession);
%>
<div class="all">
<div class="a_center">
<form action="/student/GetBlanksServlet">
    <p class="a_h2">可以这样来了解我、认识我</p>
    <div class="Choice1">
    <p>我的名字是<input type="text" name="t_name"  class="text2" value="<%=introduction.getT_name()%>">,
        性别<input type="text" name="t_gander"  class="text1" value="<%=introduction.getT_gander()%>">,
        喜欢我的朋友叫我<input type="text" name="t_nickname"  class="text2" value="<%=introduction.getT_nickname()%>">。
        <br/>
        我来自<input type="text" name="t_province"  class="text1" value="<%=introduction.getT_province()%>"> 省
        <input type="text" name="t_city"  class="text1" value="<%=introduction.getT_city()%>">市<input type="text" name="t_area"  class="text2" value="<%=introduction.getT_area()%>"> ，
        毕业于 <input type="text" name="t_highschool"  class="text2">（学校），
        <br/>
        高中主要任职<input type="text" name="t_rank"  class="text2" value="<%=introduction.getT_rank()%>">。
        现在被录取为<input type="text" name="t_major"  class="text2" value="<%=introduction.getT_major()%>">专业的2015级学生。
        <br/>
        我的QQ：<input type="text" name="t_qq"  class="text2" value="<%=introduction.getT_qq()%>">
        我的微信<input type="text" name="t_wechat"  class="text2" value="<%=introduction.getT_wechat()%>">
        在家使用的手机号码<input type="text" name="t_phone_in_home"  class="text2" value="<%=introduction.getT_phone_in_home()%>">
    </p>
        </div>
    <p class="a_h3">关于我的一些特质：</p>
    <div class="Choice1">
    <p>我一般会选择<input type="text" name="t_color"  class="text1" value="<%=introduction.getT_color()%>">色来代表自己，
        因为<input type="text" name="t_color_reason"  class="text3" value="<%=introduction.getT_color_reason()%>">。<br/>
        最擅长/最自豪/最感兴趣的是<input type="text" name="t_interests"  class="text2" value="<%=introduction.getT_interests()%>">；
        <br/>
        最喜欢的食物时<input type="text" name="t_food"  class="text2" value="<%=introduction.getT_food()%>">；
        <br/>
        喜欢的一部电影是<input type="text" name="t_movie"  class="text2" value="<%=introduction.getT_movie()%>">，
        因为<input type="text" name="t_movie_reason"  class="text3" value="<%=introduction.getT_movie_reason()%>">；
        <br/>
        最喜欢的一首歌是<input type="text" name="t_song"  class="text2" value="<%=introduction.getT_song()%>">，
        因为<input type="text" name="t_song_reason"  class="text3" value="<%=introduction.getT_song_reason()%>">。
    </p>
        </div>
    <p class="a_h3">关于我的家庭：</p>
    <div class="Choice1">
    <p>我家固定电话：<input type="text" name="t_phone1"  class="text1" value="<%=introduction.getT_phone1()%>">-<input type="text" name="t_phone2"  class="text2" value="<%=introduction.getT_phone2()%>">
        <br/>
    我家住在：<input type="text" name="t_home_province"  class="text2" value="<%=introduction.getT_home_province()%>">省
        <input type="text" name="t_home_city"  class="text1" value="<%=introduction.getT_home_city()%>">市
        <input type="text" name="t_home_area"  class="text1" value="<%=introduction.getT_home_area()%>">
    邮编：<input type="text" name="t_mailcodes"  class="text2" value="<%=introduction.getT_mailcodes()%>">
        <br/>
    父亲姓名：<input type="text" name="t_father_name"  class="text1" value="<%=introduction.getT_father_name()%>">
    职业：<input type="text" name="t_father_job"  class="text1" value="<%=introduction.getT_father_jod()%>">
    手机：<input type="text" name="t_father_phone"  class="text2" value="<%=introduction.getT_father_phone()%>">
        <br/>
    母亲姓名：<input type="text" name="t_mother_name"  class="text1" value="<%=introduction.getT_mother_name()%>">
    职业：<input type="text" name="t_mother_job"  class="text1" value="<%=introduction.getT_mother_jod()%>">
    手机：<input type="text" name="t_mother_phone"  class="text2" value="<%=introduction.getT_mother_phone()%>">
        <br/>
    此外，我还有这些亲人：<input type="text" name="t_relatives"  class="text3" value="<%=introduction.getT_relatives()%>">。
        <br/>
    家人和朋友认为我是一个<input type="text" name="t_kindof"  class="text1" value="<%=introduction.getT_kindof()%>">的人，
    而我认为如果要我选择一句话来描述自己，那会是<input type="text" name="t_describe"  class="text3" value="<%=introduction.getT_describe()%>">。</p>
        </div>
  <p class="a_h2">作为新生，其实我……</p>
    <div class="Choice1">
    <p>
        我最需要的帮助是：<input type="text" name="t_need"  class="text4" value="<%=introduction.getT_need()%>">
        <br/>
        我最想了解的是：<input type="text" name="t_understand"  class="text4" value="<%=introduction.getT_understand()%>">
        <br/>
        我最想认识的人是：<input type="text" name="t_konw"  class="text4" value="<%=introduction.getT_konw()%>">
        <br/>
        其它：<input type="text" name="t_else"  class="text4" value="<%=introduction.getT_else()%>">
    </p>
        </div>
    <br>
    <HR align=center width=auto color=#987cb9 SIZE=3>
    <div>
        <input class="testButton"type="submit" value="提交"/>
        <input class="testButton"type="reset" value="重置"/>
    </div>
</form>
</div>
</div>
</body>
</html>