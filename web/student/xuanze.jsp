<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=utf-8">--%>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%--<meta http-equiv="X-UA-Compatible" content="IE=edge" />--%>
    <title>无标题文档</title>
    <link href="../css/normalize.css" rel="stylesheet"/>
    <link href="../css/jquery-ui.css" rel="stylesheet"/>

    <link href="css/jquery.idealforms.min.css" rel="stylesheet" media="screen"/>
    <%@include file="nav.jsp"%>
    <link href="../css/xuanze.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <style type="text/css">
        .all{font:normal 15px/1.5 Arial, Helvetica, Free Sans, sans-serif;color: #222;overflow-y:hidden;padding:60px 0 0 0;}
        #my-form{width:755px;margin:0 auto;border:1px solid #ccc;padding:3em;border-radius:3px;box-shadow:0 0 2px rgba(0,0,0,.2);}
        #comments{width:350px;height:100px;}
    </style>
    <div class="center-block a_center">
        <img src="../images/zjun.jpg" class="img-responsive img-circle tm-border center-block" alt="templatemo easy profile"></div>



</head>
<body>

<div class="all">

<div class="a_center">
    <form action="/student/GetChooseServlet" method="post">
        <div id="center" class="text-left">
        <div id="left">
    <p  class="text-center a_h2">个人发展规划</p>
    <p class="a_h3">1.大学生活的主要目标是_______。</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans1 value="A" checked>A 提高综合素质，培养各种能力
            <input type=radio name=ans1 value="B">B 学习专业知识，培养专业技能
            <input type=radio name=ans1 value="C">C 结识更多朋友
            <input type=radio name=ans1 value="D">D 进入更高层次的学校继续深造
        </div>
            <br>
        <p class="a_h3">2．大学本科毕业时你的首选是__________。</p>
            <br>
        <div class="Choice1">
            <input type=radio name=ans2 value="A">A 立即工作
            <input type=radio name=ans2 value="B">B 在国内读研究生
            <input type=radio name=ans2 value="C">C 在国内读研究生
            <input type=radio name=ans2 value="D">D 其他<input type="text" name="extra2"  size="20" style="border:0px;border-bottom:#000000 1px solid;">
        </div>
            <br>
        <p class="a_h3">3．家长对你大学毕业的期望是_______。 </p>
            <br>
        <div class="Choice1">
        <input type=radio name=ans3 value="A">A 就业
        <input type=radio name=ans3 value="B">B 选择在国内攻读研究生
        <input type=radio name=ans3 value="C">C 准备出国出境留学
        <input type=radio name=ans3 value="D">D 其他<input type="text" name="extra3"  size="20" style="border:0px;border-bottom:#000000 1px solid;">
    </div>
            <br>
    <p class="a_h3">4．家长对你的要求与你的人生发展目标是否一致？ </p>
            <br>
            <div class="Choice1">
        <input type=radio name=ans4 value="A">A 一致
        <input type=radio name=ans4 value="B">B 不一致（如不一致，家长是否会尊重你的选择？<input type="text" name="extra4"  size="20" style="border:0px;border-bottom:#000000 1px solid;">)
    </div>
    <p class="a_h3">5.你的家庭经济情况能否支持你在毕业后继续读研深造（如出国、读研等）？ </p>
            <br>
            <div class="Choice1">
        <input type=radio name=ans5 value="A">A 完全能支持
        <input type=radio name=ans5 value="B">B 有一定影响
        <input type=radio name=ans5 value="C">C 不能支持
    </div>
            </div>
            <br>
<div id="right">
    <p  class="text-center a_h2">个人生活习惯及兴趣爱好调查</p>
    <div id="Choice2"><p class="a_h3">1．当我学习的时候我喜欢的环境是：</p>
        <br>
        <div class="Choice1">
        <input type=radio name=ans6 value="A">A 一个只有我一个人安静的地方
        <input type=radio name=ans6 value="B">B 和其他人一起学习（如教室或图书馆）
        <input type=radio name=ans6 value="C">C 一个只有我一个人但是有一点音乐或电视的背景的环境
    </div>
        <br>
    <p class="a_h3">2．我一般喜欢住的房间是:</p>
        <br>
        <div class="Choice1">
        <input type=radio name=ans7 value="A">A 干净，但不会总是打扫
        <input type=radio name=ans7 value="B">B 不是很干净，甚至有时有点脏乱
        <input type=radio name=ans7 value="C">C 非常整洁干净
    </div>
        <br>
    <p class="a_h3">3．我是一个___________ </p>
        <br>
        <div class="Choice1">
    <input type=radio name=ans8 value="A">A 早睡早起的人（晚上十一点前入睡）
    <input type=radio name=ans8 value="B">B 夜猫子（晚上十一点之后入睡）
     </div>
        <br>
        <p class="a_h3">4．你的睡眠好么（比如睡眠较浅，寝室稍有动静就容易醒过来等）？</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans9  value="A">A 好
            <input type=radio name=ans9 value="B">B 不好
        </div>
        <br>
        <p class="a_h3">5．我希望和我住在一起的人_______</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans10 value="A">A 和我相似
            <input type=radio name=ans10 value="B">B 和我不同
            <input type=radio name=ans10 value="C"> 无所谓
        </div>
        <br>
        <p class="a_h3">6．我希望我的寝室是：</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans11 value="A">A 充满人气，喜欢有很多人在周围
            <input type=radio name=ans11 value="B">B 适度社交，会带朋友来玩但不太频繁
            <input type=radio name=ans11 value="C">C 安静有保留的，我的寝室是我的圣地，我需要自己的隐私
        </div>
        <br>
        <p class="a_h3">7．你希望进入什么样的学生组织：</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans12 value="A">A 学生会
            <input type=radio name=ans12 value="B">B 社管会
            <input type=radio name=ans12 value="C">C 专业学习相关的社团
            <input type=radio name=ans12 value="D">D 音乐社团
            <input type=radio name=ans12 value="E">E 体育社团
        </div>
        <br>
        <p class="a_h3">8．进入大学，你觉得自己在哪方面最需要帮助：</p>
        <br>
        <div class="Choice1">
            <input type=radio name=ans13 value="A">A 学习指导
            <input type=radio name=ans13 value="B">B 生活帮助
            <input type=radio name=ans13 value="C">C 学生工作指导
            <input type=radio name=ans13 value="D">D 人际交往指点
            <input type=radio name=ans13 value="E">E 其他请说明：<input type="text" name="extra3"  size="50" style="border:0px;border-bottom:#000000 1px solid;">
        </div>
        </div>
        </div>
        </div>
        <br>
        <HR align=center width=auto color=#987cb9 SIZE=3>
        <div>
    <input class="testButton"type="submit" value="提交"/>
    <input class="testButton"type="reset" value="重置"/>
        </div>
        </form>

</div>

</div>
</body>
</html>