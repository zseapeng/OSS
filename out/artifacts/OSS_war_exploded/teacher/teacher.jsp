<%@ page import="com.zjnu.oss.model.Student" %>
<%@ page import="com.zjnu.oss.model.Teacher" %>
<%@ page import="com.zjnu.oss.util.MyBatisUtil" %>
<%@ page import="org.apache.ibatis.session.SqlSession" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.zjnu.oss.servlet.GetStudentsServlet" %><%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/22
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="../header.html"%>
    <title>teacher</title>
</head>
<body>
<%@include file="nav.jsp"%>
<%
    Teacher teacher = (Teacher) session.getAttribute("LoginUser");
    List<Student> studentList = new ArrayList<Student>();
    String classid = teacher.getT_classid();
    SqlSession sqlSession = MyBatisUtil.createSession();
    studentList = sqlSession.selectList(Student.class.getName()+".selectinfo",classid);
    MyBatisUtil.closeSqlSession(sqlSession);
//    session.setAttribute("studentList",studentList);
//    studentList = (List<Student>) session.getAttribute("studentList");
%>
<div class="container" style="margin-top: 5px">
   <table class="table table-striped table-bordered table-hover">
    <tr>
        <th >姓名</th>
        <th>性别</th>
        <th>专业</th>
        <th>班级</th>
        <th>填写状态</th>
    </tr>
       <%
           for (Student s:studentList){
               if (s.getT_status()==2){
               %>
       <tr class="success">
           <td ><a href="/teacher/GetInfoServlet?username=<%=s.getT_username()%>"><%=s.getT_name()%></a></td>
           <td><%=s.getT_gender()%></td>
           <td><%=s.getT_major()%></td>
           <td><%=s.getT_class()%></td>
           <td>已完成</td>
       </tr>
       <%
               }
               else {%>

       <tr class="danger">
           <td ><a href="/teacher/GetInfoServlet?username=<%=s.getT_username()%>"><%=s.getT_name()%></a></td>
           <td><%=s.getT_gender()%></td>
           <td><%=s.getT_major()%></td>
           <td><%=s.getT_class()%></td>
           <td>未完成</td>
       </tr>
       <%
               }
           }
       %>

   </table>
</div>
</body>
</html>
