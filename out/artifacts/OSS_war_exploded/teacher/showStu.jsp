<%@ page import="com.zjnu.oss.model.Introduction" %>
<%@ page import="javax.print.attribute.standard.DocumentName" %><%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/24
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>showstu</title>
<%@include file="../header.html"%>
</head>
<body>
<%
    Introduction introduction = new Introduction();
    introduction = (Introduction) request.getAttribute("info");
%>
<br/>
<div class="container">
    <p>我的名字是&nbsp;<%=introduction.getT_name()%>,
        <%--性别&nbsp;<%=introduction.getT_gender()%>,--%>
        喜欢我的朋友叫我<%=introduction.getT_nickname()%>。
        <br/>
        我来自<%=introduction.getT_province()%> 省
        <%=introduction.getT_city()%>市<%=introduction.getT_area()%> ，
        毕业于 <%=introduction.getT_highschool()%>（学校），
        高中主要任职<%=introduction.getT_rank()%>。
        现在被录取为<%=introduction.getT_major()%>专业的2015级学生。
        <br/>
        我的QQ：<%=introduction.getT_qq()%>
        我的微信<%=introduction.getT_wechat()%>
        在家使用的手机号码<%=introduction.getT_phone_in_home()%>
    </p>
    <h3>关于我的一些特质：</h3>
    <p>我一般会选择<%=introduction.getT_color()%>色来代表自己，
        因为<%=introduction.getT_color_reason()%>。<br/>
        最擅长/最自豪/最感兴趣的是<%=introduction.getT_interests()%>；
        <br/>
        最喜欢的食物时<%=introduction.getT_food()%>；
        <br/>
        喜欢的一部电影是<%=introduction.getT_movie()%>，
        因为<%=introduction.getT_movie_reason()%>；
        <br/>
        最喜欢的一首歌是<%=introduction.getT_song()%>，
        因为<%=introduction.getT_song_reason()%>。
    </p>
    <h3>关于我的家庭：</h3>
    <p>我家固定电话：<%=introduction.getT_phone1()%>-<%=introduction.getT_phone2()%>
        <br/>
        我家住在：<%=introduction.getT_home_province()%>省
        <%=introduction.getT_home_city()%>市
        <%=introduction.getT_home_area()%>
        邮编：<%=introduction.getT_mailcodes()%>
        <br/>
        父亲姓名：<%=introduction.getT_father_name()%>
        职业：<%=introduction.getT_father_jod()%>
        手机：<%=introduction.getT_father_phone()%>
        <br/>
        母亲姓名：<%=introduction.getT_mother_name()%>
        职业：<%=introduction.getT_mother_jod()%>
        手机：<%=introduction.getT_mother_phone()%>
        <br/>
        此外，我还有这些亲人：<%=introduction.getT_relatives()%>。
        <br/>
        家人和朋友认为我是一个<%=introduction.getT_kindof()%>的人，
        而我认为如果要我选择一句话来描述自己，那会是<%=introduction.getT_describe()%>。</p>
    <h2>作为新生，其实我……</h2>
    <p>
        我最需要的帮助是：<%=introduction.getT_need()%>
        <br/>
        我最想了解的是：<%=introduction.getT_understand()%>
        <br/>
        我最想认识的人是：<%=introduction.getT_konw()%>
        <br/>
        其它：<%=introduction.getT_else()%>
    </p>
</div>
    <%--<%--%>
        <%--response.setHeader("Content-Disposition", "attachment;filename=111.doc");--%>
    <%--%>--%>
</body>
</html>
