<%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/22
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="header.html" %>
    <title>$Title$</title>
    <style>
        *{
            padding: 5px;

        }
    </style>
</head>
<body>
<div class="container center-block">
    <form role="form" class="form-horizontal" action="LoginServlet" method="post">
        <div class="form-group">
            <label class="col-sm-offset-3 col-sm-1 control-label">帐号</label>
            <div class="col-sm-5">
                <input class="form-control" type="text" placeholder="请输入帐号" name="username">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-offset-3 col-sm-1 control-label">密码</label>
            <div class="col-sm-5">
                <input class="form-control" type="password" placeholder="请输入密码" name="password">
            </div>
        </div>
        <div class="radio col-sm-offset-4 ">
            <label>
                <input type="radio" name="type" value="2" checked/>学生
            </label>
            <label>
                <input type="radio" name="type" value="0"/>教务处
            </label>
            <label>
                <input type="radio" name="type" value="1"/>班主任
            </label>
        </div>
        <input type="submit" class="btn btn-default col-sm-offset-4" value="登录">
    </form>
</div>
</body>
</html>
