<%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/24
  Time: 8:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap.css">
</head>
<body>
<nav class="navbar  navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="col-sm-offset-5 col-sm-3">
        <ul class="nav nav-pills ">
                <li role="presentation" class="h4"><a href="student.jsp">首页</a> </li>
                <li role="presentation" class="h4"><a href="tiankong.jsp">自我介绍</a></li>
                <li role="presentation" class="h4"><a href="xuanze.jsp">大学规划</a></li>
        </ul>
        </div>
    </div>
</nav>
<script type="text/javascript" src="../js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>

