<%@ page import="com.zjnu.oss.model.Student" %><%--
  Created by IntelliJ IDEA.
  User: zseapeng
  Date: 2016/7/23
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="../header.html"%>
    <title>Title</title>
</head>
<body>
<%!
    String picurl = "../images/photo.jpg";
%>
<%
    Student student = (Student) session.getAttribute("LoginUser");
    if (!student.getT_picture().equals("")){
        picurl = student.getT_picture();
    }
%>
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-md-3">
            <a>
                <img src="<%=picurl%>">
            </a>
            
        </div>
    </div>

</div>
<form name="itemForm"   id="itemForm" method="post" action="/student/UpLoadPicServlet" enctype="multipart/form-data" onsubmit="checkForm()">
    <input name = "fileName" type ="file" class ="text1" size ="40" maxlength="40"/>
    <input type="submit" value="提交"/>
</form>

</body>
</html>
