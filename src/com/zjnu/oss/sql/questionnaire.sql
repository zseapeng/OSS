/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : questionnaire

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2016-07-23 10:03:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_intention`
-- ----------------------------
DROP TABLE IF EXISTS `t_intention`;
CREATE TABLE `t_intention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_username` varchar(20) NOT NULL,
  `t_qus_num` int(2) DEFAULT NULL,
  `t_answer` varchar(1) DEFAULT NULL,
  `t_extra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_stuid` (`t_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_intention
-- ----------------------------

-- ----------------------------
-- Table structure for `t_introduction`
-- ----------------------------
DROP TABLE IF EXISTS `t_introduction`;
CREATE TABLE `t_introduction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_username` varchar(20) NOT NULL,
  `t_name` varchar(255) NOT NULL,
  `t_gander` varchar(255) NOT NULL,
  `t_nickname` varchar(255) DEFAULT NULL,
  `t_province` varchar(255) DEFAULT NULL,
  `t_city` varchar(255) DEFAULT NULL,
  `t_area` varchar(255) DEFAULT NULL,
  `t_highschool` varchar(255) DEFAULT NULL,
  `t_rank` varchar(255) DEFAULT NULL,
  `t_major` varchar(255) DEFAULT NULL,
  `t_qq` int(11) DEFAULT NULL,
  `t_wechat` varchar(255) DEFAULT NULL,
  `t_phone_in_home` int(11) DEFAULT NULL,
  `t_color` varchar(255) DEFAULT NULL,
  `t_color_reason` varchar(255) DEFAULT NULL,
  `t_interests` varchar(255) DEFAULT NULL,
  `t_food` varchar(255) DEFAULT NULL,
  `t_movie` varchar(255) DEFAULT NULL,
  `t_movie_reason` varchar(255) DEFAULT NULL,
  `t_song` varchar(255) DEFAULT NULL,
  `t_song_reason` varchar(255) DEFAULT NULL,
  `t_phone1` varchar(255) DEFAULT NULL,
  `t_phone2` varchar(255) DEFAULT NULL,
  `t_home_province` varchar(255) DEFAULT NULL,
  `t_home_city` varchar(255) DEFAULT NULL,
  `t_home_area` varchar(255) DEFAULT NULL,
  `t_mailcodes` varchar(255) DEFAULT NULL,
  `t_father_name` varchar(255) DEFAULT NULL,
  `t_father_job` varchar(255) DEFAULT NULL,
  `t_father_phone` varchar(255) DEFAULT NULL,
  `t_mother_name` varchar(255) DEFAULT NULL,
  `t_mother_job` varchar(255) DEFAULT NULL,
  `t_mother_phone` varchar(255) DEFAULT NULL,
  `t_relatives` varchar(255) DEFAULT NULL,
  `t_kindof` varchar(255) DEFAULT NULL,
  `t_describe` varchar(255) DEFAULT NULL,
  `t_need` varchar(255) DEFAULT NULL,
  `t_understand` varchar(255) DEFAULT NULL,
  `t_konw` varchar(255) DEFAULT NULL,
  `t_else` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_stuid` (`t_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_introduction
-- ----------------------------

-- ----------------------------
-- Table structure for `t_login_major`
-- ----------------------------
DROP TABLE IF EXISTS `t_login_major`;
CREATE TABLE `t_login_major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_username` varchar(20) COLLATE utf8_bin NOT NULL,
  `t_password` varchar(30) COLLATE utf8_bin NOT NULL,
  `t_type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_login_major
-- ----------------------------

-- ----------------------------
-- Table structure for `t_login_stu`
-- ----------------------------
DROP TABLE IF EXISTS `t_login_stu`;
CREATE TABLE `t_login_stu` (
  `id` int(11) NOT NULL,
  `t_username` varchar(20) NOT NULL,
  `t_classid` varchar(20) NOT NULL,
  `t_password` varchar(30) NOT NULL,
  `t_type` int(1) NOT NULL,
  `t_name` varchar(255) NOT NULL,
  `t_gender` varchar(255) NOT NULL,
  `t_major` varchar(255) NOT NULL,
  `t_class` varchar(255) NOT NULL,
  `t_picture` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `t_stu_classid` (`t_classid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_login_stu
-- ----------------------------

-- ----------------------------
-- Table structure for `t_login_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `t_login_teacher`;
CREATE TABLE `t_login_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `t_username` varchar(20) COLLATE utf8_bin NOT NULL,
  `t_classid` varchar(20) COLLATE utf8_bin NOT NULL,
  `t_password` varchar(30) COLLATE utf8_bin NOT NULL,
  `t_type` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `t_teacher_classid` (`t_classid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of t_login_teacher
-- ----------------------------
