package com.zjnu.oss.util;

import com.zjnu.oss.model.Person;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by zseapeng on 2016/7/22.
 */
public class TeacherCheckFilterUtil implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpServletRequest.getSession(true);
        Person person = (Person) session.getAttribute("LoginUser");
        if (person==null){
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/error.jsp");
            return;
        }else if (person.getT_type()!=1){
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/error.jsp");
            return;
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
