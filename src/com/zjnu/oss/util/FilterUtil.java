package com.zjnu.oss.util;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by zseapeng on 2016/7/12.
 */
public class FilterUtil implements Filter {
    private String encoding;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
            String temp = filterConfig.getInitParameter("encoding");
            if (temp == null||temp.equals("")){
                encoding = "utf-8";
            }else {
                encoding = temp;
            }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(encoding);
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
