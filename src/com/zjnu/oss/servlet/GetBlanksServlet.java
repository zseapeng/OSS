package com.zjnu.oss.servlet;

import com.zjnu.oss.model.Introduction;
import com.zjnu.oss.model.Student;
import com.zjnu.oss.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by zseapeng on 2016/7/25.
 */
@WebServlet(name = "GetBlanksServlet", urlPatterns = {"/student/GetBlanksServlet"})
public class GetBlanksServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        通过 request 方法得到前台的数据
//        by xyfreedomc
        HttpSession session = request.getSession();
        Student student = (Student) session.getAttribute("LoginUser");
        String username = student.getT_username();


        String t_name = request.getParameter("t_name");
        String t_gander = request.getParameter("t_gander");
        String t_nickname = request.getParameter("t_nickname");
        String t_province = request.getParameter("t_province");
        String t_city = request.getParameter("t_city");
        String t_area = request.getParameter("t_area");
        String t_highschool = request.getParameter("t_highschool");
        String t_rank = request.getParameter("t_rank");
        String t_major = request.getParameter("t_major");
        String t_qq = request.getParameter("t_qq");
        String t_wechat = request.getParameter("t_wechat");
        String t_phone_in_home = request.getParameter("t_phone_in_hone");
        String t_color = request.getParameter("t_color");
        String t_color_reason = request.getParameter("t_color_reason");
        String t_interests = request.getParameter("t_interests");
        String t_food = request.getParameter("t_food");
        String t_movie = request.getParameter("t_movie");
        String t_movie_reason = request.getParameter("t_movie_reason");
        String t_song = request.getParameter("t_song");
        String t_song_reason = request.getParameter("t_song_reason");
        String t_phone1 = request.getParameter("t_phone1");
        String t_phone2 = request.getParameter("t_phone2");
        String t_home_province = request.getParameter("t_home_province");
        String t_home_city = request.getParameter("t_home_city");
        String t_home_area = request.getParameter("t_home_area");
        String t_mailcodes = request.getParameter("t_mailcodes");
        String t_father_name = request.getParameter("t_father_name");
        String t_father_job = request.getParameter("t_father_job");
        String t_father_phone = request.getParameter("t_father_phone");
        String t_mother_name = request.getParameter("t_mother_name");
        String t_mother_job = request.getParameter("t_mother_job");
        String t_mother_phone = request.getParameter("t_mother_phone");
        String t_relatives = request.getParameter("t_relatives");
        String t_kindof = request.getParameter("t_kingof");
        String t_describe = request.getParameter("t_describe");
        String t_need = request.getParameter("t_need");
        String t_understand = request.getParameter("t_understand");
        String t_konw = request.getParameter("t_know");
        String t_else = request.getParameter("t_else");

        Introduction introduction = new Introduction();

        introduction.setT_username(username);

        introduction.setT_name(t_name);
        introduction.setT_gander(t_gander);
        introduction.setT_nickname(t_nickname);
        introduction.setT_province(t_province);
        introduction.setT_city(t_city);
        introduction.setT_area(t_area);
        introduction.setT_highschool(t_highschool);
        introduction.setT_rank(t_rank);
        introduction.setT_major(t_major);
        introduction.setT_qq(t_qq);
        introduction.setT_wechat(t_wechat);
        introduction.setT_phone_in_home(t_phone_in_home);
        introduction.setT_color(t_color);
        introduction.setT_color_reason(t_color_reason);
        introduction.setT_interests(t_interests);
        introduction.setT_food(t_food);
        introduction.setT_movie(t_movie);
        introduction.setT_movie_reason(t_movie_reason);
        introduction.setT_song(t_song);
        introduction.setT_song_reason(t_song_reason);
        introduction.setT_phone1(t_phone1);
        introduction.setT_phone2(t_phone2);
        introduction.setT_home_province(t_home_province);
        introduction.setT_home_city(t_home_city);
        introduction.setT_home_area(t_home_area);
        introduction.setT_mailcodes(t_mailcodes);
        introduction.setT_father_name(t_father_name);
        introduction.setT_father_jod(t_father_job);
        introduction.setT_father_phone(t_father_phone);
        introduction.setT_mother_name(t_mother_name);
        introduction.setT_mother_jod(t_mother_job);
        introduction.setT_mother_phone(t_mother_phone);
        introduction.setT_relatives(t_relatives);
        introduction.setT_kindof(t_kindof);
        introduction.setT_describe(t_describe);
        introduction.setT_need(t_need);
        introduction.setT_understand(t_understand);
        introduction.setT_konw(t_konw);
        introduction.setT_else(t_else);

        System.out.print("username= "+introduction.getT_username());
        System.out.println(introduction.getT_song());
        SqlSession sqlSession = MyBatisUtil.createSession();
        System.out.println("sqlinfo= "+sqlSession.update(Introduction.class.getName()+".update",introduction));
        sqlSession.commit();
        MyBatisUtil.closeSqlSession(sqlSession);
        session.setAttribute("introduction",introduction);
        System.out.println(request.getContextPath());
        //response.sendRedirect(request.getContextPath()+"/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
