package com.zjnu.oss.servlet;

import com.zjnu.oss.model.Intention;
import com.zjnu.oss.model.Student;
import com.zjnu.oss.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by xyfreedomc on 2016/7/26.
 */
@WebServlet(name = "GetChooseServlet", urlPatterns = {"/student/GetChooseServlet"})
public class GetChooseServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        通过 request 方法得到前台的数据
//        by xyfreedomc

        HttpSession session = req.getSession();
        Student student = (Student) session.getAttribute("LoginUser");
        String username = student.getT_username();

        String ans1 = req.getParameter("ans1");
        String ans2 = req.getParameter("ans2");
        String extra2 = req.getParameter("extra2");
        String ans3 = req.getParameter("ans3");
        String extra3 = req.getParameter("extra3");
        String ans4 = req.getParameter("ans4");
        String extra4 = req.getParameter("extra4");
        String ans5 = req.getParameter("ans5");
        String ans6 = req.getParameter("ans6");
        String ans7 = req.getParameter("ans7");
        String ans8 = req.getParameter("ans8");
        String ans9 = req.getParameter("ans9");
        String ans10 = req.getParameter("ans10");
        String ans11 = req.getParameter("ans11");
        String ans12 = req.getParameter("ans12");
        String ans13 = req.getParameter("ans13");
        String extra13 = req.getParameter("extra13");

        Intention intention = new Intention();
        intention.setT_username(username);
        intention.setT_ans1(ans1);
        intention.setT_ans2(ans2);
        intention.setT_extra2(extra2);
        intention.setT_ans3(ans3);
        intention.setT_extra3(extra3);
        intention.setT_ans4(ans4);
        intention.setT_extra4(extra4);
        intention.setT_ans5(ans5);
        intention.setT_ans6(ans6);
        intention.setT_ans7(ans7);
        intention.setT_ans8(ans8);
        intention.setT_ans9(ans9);
        intention.setT_ans10(ans10);
        intention.setT_ans11(ans11);
        intention.setT_ans12(ans12);
        intention.setT_ans13(ans13);
        intention.setT_extra13(extra13);
        System.out.println(ans4);
        SqlSession sqlSession = MyBatisUtil.createSession();
        if (sqlSession.insert(Intention.class.getName()+".updateinfo",intention)==1){
            System.out.print("添加成功");
        }
        sqlSession.commit();
        MyBatisUtil.closeSqlSession(sqlSession);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
