package com.zjnu.oss.servlet;

import com.zjnu.oss.model.Introduction;
import com.zjnu.oss.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by zseapeng on 2016/7/24.
 */
@WebServlet(name = "GetInfoServlet", urlPatterns = {"/teacher/GetInfoServlet"})
public class GetInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        System.out.println(username);
        Introduction introduction = null;
        SqlSession sqlSession = MyBatisUtil.createSession();
        introduction = sqlSession.selectOne(Introduction.class.getName()+".selectinfo",username);
        MyBatisUtil.closeSqlSession(sqlSession);
        System.out.println("hightschool= "+introduction.getT_highschool());
        request.setAttribute("info",introduction);
        RequestDispatcher requestDispatcher=request.getRequestDispatcher("/teacher/showStu.jsp");
        requestDispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            doPost(request,response);
    }
}
