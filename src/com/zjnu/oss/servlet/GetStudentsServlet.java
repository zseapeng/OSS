package com.zjnu.oss.servlet;

import com.zjnu.oss.model.Teacher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by zseapeng on 2016/7/24.
 */
@WebServlet(name = "GetStudentsServlet", urlPatterns = {"/teacher/GetStudentsServlet"})
public class GetStudentsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Teacher teacher = (Teacher) session.getAttribute("LoginUser");
        //String classid = teacher.getT_classid();
        //List<Student> studentList = new ArrayList<Student>();
        //SqlSession sqlSession = MyBatisUtil.createSession();
        //studentList = sqlSession.selectList(Student.class.getName()+".selectinfo",classid);
        //MyBatisUtil.closeSqlSession(sqlSession);
        //session.setAttribute("studentList",studentList);
        response.sendRedirect(request.getContextPath()+"/teacher/teacher.jsp");
        return;
        //RequestDispatcher requestDispatcher=request.getRequestDispatcher("/teacher/teacher.jsp");
        //requestDispatcher.forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
