package com.zjnu.oss.model;

/**
 * Created by zseapeng on 2016/7/22.
 */
public class Admin  extends Person{
    public Admin() {
    }

    public Admin(String t_username, String t_password, int t_type) {
        super(t_username, t_password, t_type);
    }
}
