package com.zjnu.oss.model;

public class Introduction {
	private int id;
	private String t_username;
	private String t_name;
	private String t_gander;
	private String t_nickname;
	private String t_province;
	private String t_city;
	private String t_area;
	private String t_highschool;
	private String t_rank;
	private String t_major;
	private String t_qq;
	private String t_wechat;
	private String t_phone_in_home;
	private String t_color;
	private String t_color_reason;
	private String t_interests;
	private String t_food;
	private String t_movie;
	private String t_movie_reason;
	private String t_song;
	private String t_song_reason;
	private String t_phone1;
	private String t_phone2;
	private String t_home_province;
	private String t_home_city;
	private String t_home_area;
	private String t_mailcodes;
	private String t_father_name;
	private String t_father_jod;
	private String t_father_phone;
	private String t_mother_name;
	private String t_mother_jod;
	private String t_mother_phone;
	private String t_relatives;
	private String t_kindof;
	private String t_describe;
	private String t_need;
	private String t_understand;
	private String t_konw;                              //???
	private String t_else;
	
	
	
	
	public int getId() {
		return id;
	}
	public String getT_username() {
		return t_username;
	}
	public String getT_name() {
		return t_name;
	}
	public String getT_gander() {
		return t_gander;
	}
	public String getT_nickname() {
		return t_nickname;
	}
	public String getT_province() {
		return t_province;
	}
	public String getT_city() {
		return t_city;
	}
	public String getT_area() {
		return t_area;
	}
	public String getT_highschool() {
		return t_highschool;
	}
	public String getT_rank() {
		return t_rank;
	}
	public String getT_major() {
		return t_major;
	}
	public String getT_qq() {
		return t_qq;
	}
	public String getT_wechat() {
		return t_wechat;
	}
	public String getT_phone_in_home() {
		return t_phone_in_home;
	}
	public String getT_color() {
		return t_color;
	}
	public String getT_color_reason() {
		return t_color_reason;
	}
	public String getT_interests() {
		return t_interests;
	}
	public String getT_food() {
		return t_food;
	}
	public String getT_movie() {
		return t_movie;
	}
	public String getT_movie_reason() {
		return t_movie_reason;
	}
	public String getT_song() {
		return t_song;
	}
	public String getT_song_reason() {
		return t_song_reason;
	}
	public String getT_phone1() {
		return t_phone1;
	}
	public String getT_phone2() {
		return t_phone2;
	}
	public String getT_home_province() {
		return t_home_province;
	}
	public String getT_home_city() {
		return t_home_city;
	}
	public String getT_home_area() {
		return t_home_area;
	}
	public String getT_mailcodes() {
		return t_mailcodes;
	}
	public String getT_father_name() {
		return t_father_name;
	}
	public String getT_father_jod() {
		return t_father_jod;
	}
	public String getT_father_phone() {
		return t_father_phone;
	}
	public String getT_mother_name() {
		return t_mother_name;
	}
	public String getT_mother_jod() {
		return t_mother_jod;
	}
	public String getT_mother_phone() {
		return t_mother_phone;
	}
	public String getT_relatives() {
		return t_relatives;
	}
	public String getT_kindof() {
		return t_kindof;
	}
	public String getT_describe() {
		return t_describe;
	}
	public String getT_need() {
		return t_need;
	}
	public String getT_understand() {
		return t_understand;
	}
	public String getT_konw() {                                  //
		return t_konw;
	}
	public String getT_else() {
		return t_else;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	public void setT_username(String t_username) {
		this.t_username = t_username;
	}
	public void setT_name(String t_name) {
		this.t_name = t_name;
	}
	public void setT_gander(String t_gander) {
		this.t_gander = t_gander;
	}
	public void setT_nickname(String t_nickname) {
		this.t_nickname = t_nickname;
	}
	public void setT_province(String province) {
		this.t_province = province;
	}
	public void setT_city(String t_city) {
		this.t_city = t_city;
	}
	public void setT_area(String t_area) {
		this.t_area = t_area;
	}
	public void setT_highschool(String t_highschool) {
		this.t_highschool = t_highschool;
	}
	public void setT_rank(String t_rank) {
		this.t_rank = t_rank;
	}
	public void setT_major(String t_major) {
		this.t_major = t_major;
	}
	public void setT_wechat(String t_wechat) {
		this.t_wechat = t_wechat;
	}
	public void setT_phone_in_home(String t_phone_in_home) {
		this.t_phone_in_home = t_phone_in_home;
	}
	public void setT_color(String t_color) {
		this.t_color = t_color;
	}
	public void setT_color_reason(String t_color_reason) {
		this.t_color_reason = t_color_reason;
	}
	public void setT_interests(String t_interests) {
		this.t_interests = t_interests;
	}
	public void setT_food(String t_food) {
		this.t_food = t_food;
	}
	public void setT_movie(String t_movie) {
		this.t_movie = t_movie;
	}
	public void setT_movie_reason(String t_movie_reason) {
		this.t_movie_reason = t_movie_reason;
	}
	public void setT_song(String t_song) {
		this.t_song = t_song;
	}
	public void setT_song_reason(String t_song_reason) {
		this.t_song_reason = t_song_reason;
	}
	public void setT_phone1(String t_phone1) {
		this.t_phone1 = t_phone1;
	}
	public void setT_phone2(String t_phone2) {
		this.t_phone2 = t_phone2;
	}
	public void setT_home_province(String t_home_province) {
		this.t_home_province = t_home_province;
	}
	public void setT_home_city(String t_home_city) {
		this.t_home_city = t_home_city;
	}
	public void setT_home_area(String t_home_area) {
		this.t_home_area = t_home_area;
	}
	public void setT_mailcodes(String t_mailcodes) {
		this.t_mailcodes = t_mailcodes;
	}
	public void setT_father_name(String t_father_name) {
		this.t_father_name = t_father_name;
	}
	public void setT_father_jod(String t_father_jod) {
		this.t_father_jod = t_father_jod;
	}
	public void setT_father_phone(String t_father_phone) {
		this.t_father_phone = t_father_phone;
	}
	public void setT_mother_name(String t_mother_name) {
		this.t_mother_name = t_mother_name;
	}
	public void setT_mother_jod(String t_mother_jod) {
		this.t_mother_jod = t_mother_jod;
	}
	public void setT_mother_phone(String t_mother_phone) {
		this.t_mother_phone = t_mother_phone;
	}
	public void setT_relatives(String t_relatives) {
		this.t_relatives = t_relatives;
	}
	public void setT_kindof(String t_kindof) {
		this.t_kindof = t_kindof;
	}
	public void setT_describe(String t_describe) {
		this.t_describe = t_describe;
	}
	public void setT_need(String t_need) {
		this.t_need = t_need;
	}
	public void setT_understand(String t_understand) {
		this.t_understand = t_understand;
	}
	public void setT_konw(String t_konw) {
		this.t_konw = t_konw;
	}
	public void setT_else(String t_else) {
		this.t_else = t_else;
	}

	public void setT_qq(String t_qq) {

	}
}
