package com.zjnu.oss.model;

/**
 * Created by zseapeng on 2016/7/23.
 */
public class Intention {
        private int id;
        private String t_username;
        private String t_ans1;
        private String t_ans2;
        private String t_extra2;
        private String t_ans3;
        private String t_extra3;
        private String t_ans4;
        private String t_extra4;
        private String t_ans5;
        private String t_ans6;
        private String t_ans7;
        private String t_ans8;
        private String t_ans9;
        private String t_ans10;
        private String t_ans11;
        private String t_ans12;
        private String t_ans13;
        private String t_extra13;

    public Intention() {
    }


    public int getId() {
            return id;
        }

        public String getT_username() {
            return t_username;
        }

        public String getT_ans1() {
            return t_ans1;
        }

        public String getT_ans2() {
            return t_ans2;
        }

        public String getT_extra2() {
            return t_extra2;
        }

        public String getT_ans3() {
            return t_ans3;
        }

        public String getT_ans4() {
            return t_ans4;
        }

        public String getT_extra4() {
            return t_extra4;
        }

        public String getT_ans5() {
            return t_ans5;
        }

        public String getT_ans6() {
            return t_ans6;
        }

        public String getT_ans7() {
            return t_ans7;
        }

        public String getT_ans8() {
            return t_ans8;
        }

        public String getT_ans9() {
            return t_ans9;
        }

        public String getT_ans10() {
            return t_ans10;
        }

        public String getT_ans11() {
            return t_ans11;
        }

        public String getT_ans12() {
            return t_ans12;
        }

        public String getT_ans13() {
            return t_ans13;
        }

        public String getT_extra13() {
            return t_extra13;
        }



        public void setId(int id) {
            this.id = id;
        }

        public void setT_username(String t_username) {
            this.t_username = t_username;
        }

        public void setT_ans1(String t_ans1) {
            this.t_ans1 = t_ans1;
        }

        public void setT_ans2(String t_ans2) {
            this.t_ans2 = t_ans2;
        }

        public void setT_extra2(String t_extra2) {
            this.t_extra2 = t_extra2;
        }

        public void setT_ans3(String t_ans3) {
            this.t_ans3 = t_ans3;
        }

        public void setT_ans4(String t_ans4) {
            this.t_ans4 = t_ans4;
        }

        public void setT_extra4(String t_extra4) {
            this.t_extra4 = t_extra4;
        }

        public void setT_ans5(String t_ans5) {
            this.t_ans5 = t_ans5;
        }

        public void setT_ans6(String t_ans6) {
            this.t_ans6 = t_ans6;
        }

        public void setT_ans7(String t_ans7) {
            this.t_ans7 = t_ans7;
        }

        public void setT_ans8(String t_ans8) {
            this.t_ans8 = t_ans8;
        }

        public void setT_ans9(String t_ans9) {
            this.t_ans9 = t_ans9;
        }

        public void setT_ans10(String t_ans10) {
            this.t_ans10 = t_ans10;
        }

        public void setT_ans11(String t_ans11) {
            this.t_ans11 = t_ans11;
        }

        public void setT_ans12(String t_ans12) {
            this.t_ans12 = t_ans12;
        }

        public void setT_ans13(String t_ans13) {
            this.t_ans13 = t_ans13;
        }

        public void setT_extra13(String t_extra13) {
            this.t_extra13 = t_extra13;
        }

    public String getT_extra3() {
        return t_extra3;
    }

    public void setT_extra3(String t_extra3) {
        this.t_extra3 = t_extra3;
    }
}
