package com.zjnu.oss.model;

/**
 * Created by zseapeng on 2016/7/22.
 */
public class Student extends Person{
    private int t_status;
    private String t_classid;
    private String t_name;
    private String t_gender;
    private String t_major;
    private String t_class;
    private String t_picture;

    public Student() {
    }

    public Student(String t_username, String t_password) {
        super(t_username, t_password);
    }

    public Student(String t_username, String t_password, int t_type) {
        super(t_username, t_password, t_type);
    }

    public Student(String t_username, String t_password, int t_type, int t_status, String t_classid, String t_name, String t_gender, String t_major, String t_class, String t_picture) {
        super(t_username, t_password, t_type);
        this.t_status = t_status;
        this.t_classid = t_classid;
        this.t_name = t_name;
        this.t_gender = t_gender;
        this.t_major = t_major;
        this.t_class = t_class;
        this.t_picture = t_picture;
    }

    public int getT_status() {
        return t_status;
    }

    public void setT_status(int t_status) {
        this.t_status = t_status;
    }

    public String getT_classid() {
        return t_classid;
    }

    public void setT_classid(String t_classid) {
        this.t_classid = t_classid;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public String getT_gender() {
        return t_gender;
    }

    public void setT_gender(String t_gender) {
        this.t_gender = t_gender;
    }

    public String getT_major() {
        return t_major;
    }

    public void setT_major(String t_major) {
        this.t_major = t_major;
    }

    public String getT_class() {
        return t_class;
    }

    public void setT_class(String t_class) {
        this.t_class = t_class;
    }

    public String getT_picture() {
        return t_picture;
    }

    public void setT_picture(String t_picture) {
        this.t_picture = t_picture;
    }
}
