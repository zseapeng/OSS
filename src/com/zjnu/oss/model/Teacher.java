package com.zjnu.oss.model;

/**
 * Created by zseapeng on 2016/7/22.
 */
public class Teacher extends Person {
    private String t_classid;
    public Teacher() {
    }

    public Teacher(String t_username, String t_password) {
        super(t_username, t_password);
    }

    public Teacher(String t_username, String t_password, Integer t_type) {
        super(t_username, t_password, t_type);
    }

    public String getT_classid() {
        return t_classid;
    }

    public void setT_classid(String t_classid) {
        this.t_classid = t_classid;
    }
}
