package com.zjnu.oss.model;

/**
 * Created by zseapeng on 2016/7/22.
 */
public class Person {
    private int id;
    private String t_username;
    private String t_password;
    private int t_type;

    public Person() {
    }

    public Person(String t_username, String t_password) {
        this.t_username = t_username;
        this.t_password = t_password;
    }

    public Person(String t_username, String t_password, int t_type) {
        this.t_username = t_username;
        this.t_password = t_password;
        this.t_type = t_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getT_username() {
        return t_username;
    }

    public void setT_username(String t_username) {
        this.t_username = t_username;
    }

    public String getT_password() {
        return t_password;
    }

    public void setT_password(String t_password) {
        this.t_password = t_password;
    }

    public int getT_type() {
        return t_type;
    }

    public void setT_type(int t_type) {
        this.t_type = t_type;
    }
}
